import * as types from '../actions/actionTypes';  
import currencyInitialState from './currencyInitialState';


export default function currencyReducer(state=currencyInitialState, action) {
    switch(action.type) {
        case types.GET_CURRENCY_SUCCESS:
            return Object.assign({}, state, {
                getCurrency: action.getCurrencyResponse
            });
        case types.CONVERT_CURRENCY_SUCCESS:
            return Object.assign({}, state, {
                convertCurrency: action.convertCurrencyResponse
            });
        default:
            return state;
    }
}