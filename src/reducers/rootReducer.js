import {combineReducers} from 'redux'
import * as types from '../actions/actionTypes'
import currencies from './currencyReducer'

const appReducer = combineReducers({
  // short hand property names
  currencies
});

const rootReducer = (state, action) => {
    return appReducer(state, action)
};

export default rootReducer
