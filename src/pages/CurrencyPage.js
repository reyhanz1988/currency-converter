import React, {Component} from 'react'
import {Container, Row, Col} from 'reactstrap'

import CurrencyContent from '../components/CurrencyContent'


class CurrencyPage extends Component {
    render() {
        return(
            <Container fluid>
                <Row>
                    <Col md='4'>
                        <CurrencyContent/>
                    </Col>
                </Row>
            </Container>
        )
    }
}

export default CurrencyPage