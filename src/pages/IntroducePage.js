import React, {Component} from 'react'
import {Container, Row} from 'reactstrap'

import IntroduceContent from '../components/IntroduceContent'


class IntroducePage extends Component {
    render() {
        return(
            <Container fluid>
                <Row className="main-container">
                    <IntroduceContent/>
                </Row>
            </Container>
        )
    }
}

export default IntroducePage