import * as types from './actionTypes';
import currencyApi from '../api/currencyApi';

export function getCurrency(getCurrencyResponse){
    return function(dispatch){
        return currencyApi.getCurrency(getCurrencyResponse)
            .then(
                response => {
                 dispatch(getCurrencySuccess(response));
                    return response;
                }
            ).catch(
                error => {
                    throw(error);
                }
            )
    }
}

export function getCurrencySuccess(getCurrencyResponse){
    return {
        type: types.GET_CURRENCY_SUCCESS,
        getCurrencyResponse
    }
}

export function convertCurrency(convertCurrencyResponse){
    return function(dispatch){
        return currencyApi.convertCurrency(convertCurrencyResponse)
            .then(
                response => {
                 dispatch(convertCurrencySuccess(response));
                    return response;
                }
            ).catch(
                error => {
                    throw(error);
                }
            )
    }
}

export function convertCurrencySuccess(convertCurrencyResponse){
    return {
        type: types.CONVERT_CURRENCY_SUCCESS,
        convertCurrencyResponse
    }
}