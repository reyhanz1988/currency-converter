import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';

import CurrencyPage from'./pages/CurrencyPage';
//import IntroducePage from './pages/IntroducePage';



const Routes = (
    <BrowserRouter>
        <div>
            <Route exact path="/" component={CurrencyPage}/>
            {/*<Route exact path="/introduce" component={IntroducePage}/>*/}
        </div>
    </BrowserRouter>
);

export default Routes
