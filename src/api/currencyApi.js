class CurrencyApi {
    static getCurrency() {
        let api_url = 'https://api.exchangeratesapi.io/latest?';
        const request = new Request(api_url, {
            method: 'GET',
            accept: 'application/json'
        });
        return fetch(request)
            .then(
                response => {
                    return response.json();
                }
            ).then(
                function(data) {
                    return data;
                }
            ).catch(
                error => {
                    return error;
                }
            )
    }
    static convertCurrency(base) {
        let api_url = 'https://api.exchangeratesapi.io/latest?';

        if (base!=='' && base !== null && "undefined" !== typeof base) {
            api_url = api_url + '&base=' + base;
        }
        else{
            api_url = api_url + '&base=USD';
        }
        const request = new Request(api_url, {
            method: 'GET',
            accept: 'application/json'
        });
        
        return fetch(request)
            .then(
                response => {
                    return response.json();
                }
            ).then(
                function(data) {
                    return data;
                }
            ).catch(
                error => {
                    return error;
                }
            )
    }
}

export default CurrencyApi