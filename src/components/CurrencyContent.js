import React, {Component} from 'react';
import { Button, Form, FormGroup, Label, Input, InputGroup, InputGroupAddon, Row, Col, Jumbotron} from 'reactstrap';
import NumberFormat from 'react-number-format';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as currencyActions from '../actions/currencyActions';

class CurrencyContent extends Component {
    componentDidMount() {
        this.props.currencyActions.getCurrency();
    }
    componentDidUpdate(prevprops) {
        if (prevprops.currencies.convertCurrency !== this.props.currencies.convertCurrency) {
            let exchangeRate = 0;
            let exchangeRates = this.props.currencies.convertCurrency.rates;
            for (let rateKeys in exchangeRates){
                if (exchangeRates.hasOwnProperty(rateKeys)) {
                    if(rateKeys == this.state.currencyRes){
                        exchangeRate = exchangeRates[rateKeys];
                    }
                }
            }
            let exchangeRes = 0;
            if(exchangeRate != 0){
                exchangeRes = exchangeRate*this.state.currencyFromVal;
            }
            this.setState({currencyResVal: exchangeRes});
            
        }
    }
    constructor(props) {
        super(props);
        this.state = {
            getCurrency: [],
            convertCurrency: [],
            currencyFrom: 'USD',
            currencyFromVal: 0,
            currencyRes: 'IDR',
            currencyResVal: 0,
            isLoading: false
        };
        this.convertCurrency = this.convertCurrency.bind(this);
        this.updateState = this.updateState.bind(this);
    }
    convertCurrency() {
        clearTimeout(this.searchTimer);
        this.searchTimer = setTimeout(() => {
            this.props.currencyActions.convertCurrency(this.state.currencyFrom);
        }, 500);
    }
    updateState(e) {
        if (e.target.id === "currencyFrom") {
            let currencyFromInit = document.getElementById("currencyFrom");
            let currencyFrom = currencyFromInit.options[currencyFromInit.selectedIndex].value;
            this.setState({currencyFrom: currencyFrom});
            this.convertCurrency();
        }
        if (e.target.id === "currencyFromVal") {
            this.setState({currencyFromVal: e.target.value});
            this.convertCurrency();
        }
        if (e.target.id === "currencyRes") {
            let currencyResInit = document.getElementById("currencyRes");
            let currencyRes = currencyResInit.options[currencyResInit.selectedIndex].value;
            this.setState({currencyRes: currencyRes});
            this.convertCurrency();
        }
    }
    render() {
        let currencies = this.props.currencies.getCurrency.rates;
        let countCurrencies = 0;
        if ("undefined" !== typeof currencies) {
            countCurrencies = currencies.length;
        }
        let optionValuesFrom = new Array;
        for (let currencyKeysFrom in currencies){
            if (currencies.hasOwnProperty(currencyKeysFrom)) {
                if(currencyKeysFrom == 'USD'){
                    optionValuesFrom.push(<option selected='selected'>{currencyKeysFrom}</option>);
                }
                else{
                    optionValuesFrom.push(<option>{currencyKeysFrom}</option>);
                }
            }
        }
        let optionValuesRes = new Array;
        for (let currencyKeysRes in currencies){
            if (currencies.hasOwnProperty(currencyKeysRes)) {
                if(currencyKeysRes == 'IDR'){
                    optionValuesRes.push(<option selected='selected'>{currencyKeysRes}</option>);
                }
                else{
                    optionValuesRes.push(<option>{currencyKeysRes}</option>);
                }
            }
        }
        return(
            <Col>
                <h1>Currency Apps</h1>
                <Form>
                    <Label>From</Label>
                    <InputGroup>
                        <Col md='3'>
                            <Input type="select" name="currencyFrom" onChange={this.updateState} id="currencyFrom">
                                {optionValuesFrom}
                            </Input>
                        </Col>
                        <Col md='9'>
                            <Input type="text" name="currencyFromVal" onChange={this.updateState} id="currencyFromVal" placeholder="Enter value to convert" />
                        </Col>
                    </InputGroup>
                    <Label>To</Label>
                    <InputGroup>
                        <Col md='3'>
                            <Input type="select" name="currencyRes" onChange={this.updateState} id="currencyRes">
                                {optionValuesRes}
                            </Input>
                        </Col>
                        <Col md='9'>
                            <Input disabled type="text" value={this.state.currencyResVal} name="currencyResVal" id="currencyResVal" placeholder="Result" />
                        </Col>
                    </InputGroup>
                </Form>
            </Col>
        )
    }
}

function mapStateToProps(state, ownProps) {
    return {
        currencies: state.currencies,
    }
}

function mapDispatchToProps(dispatch) {  
    return {
        currencyActions: bindActionCreators(currencyActions, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CurrencyContent)